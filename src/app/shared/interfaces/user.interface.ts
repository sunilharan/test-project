export interface User {
    id: number;
    image: string;
    name: string;
    desc?: string;
}
