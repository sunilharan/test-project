export const userList = [
  {
    id: 1,
    name: "Hobard Dewfall",
    desc: "Occupant (driver) (passenger) of three-wheeled motor vehicle injured in unspecified nontraffic accident, subsequent encounter",
    image: "https://randomuser.me/api/portraits/men/50.jpg",
  },
  {
    id: 2,
    name: "Pamelina Carley",
    desc: "Drug-induced chronic gout, left elbow, with tophus (tophi)",
    image: "https://randomuser.me/api/portraits/women/96.jpg",
  },
  {
    id: 3,
    name: "Rodrique Ingilson",
    desc: "Displaced oblique fracture of shaft of right tibia, subsequent encounter for closed fracture with routine healing",
    image: "https://randomuser.me/api/portraits/men/63.jpg",
  },
  {
    id: 4,
    name: "Gelya McCarter",
    desc: "Polyhydramnios, unspecified trimester, fetus 4",
    image: "https://randomuser.me/api/portraits/men/47.jpg",
  },
  {
    id: 5,
    name: "Dunstan Gherardelli",
    desc: "Unspecified injury of ovary, unilateral, initial encounter",
    image: "https://randomuser.me/api/portraits/men/74.jpg",
  },
  {
    id: 6,
    name: "Dicky Wolffers",
    desc: "Immersion hand, right hand",
    image: "https://randomuser.me/api/portraits/women/56.jpg",
  },
  {
    id: 7,
    name: "Ugo Gogerty",
    desc: "Other mechanical complication of prosthetic orbit of left eye",
    image: "https://randomuser.me/api/portraits/men/36.jpg",
  },
  {
    id: 8,
    name: "Gerta Staniford",
    desc: "Underdosing of other antihypertensive drugs, subsequent encounter",
    image: "https://randomuser.me/api/portraits/men/47.jpg",
  },
  {
    id: 9,
    name: "Debor Neem",
    desc: "Toxic effect of other specified gases, fumes and vapors",
    image: "https://randomuser.me/api/portraits/women/29.jpg",
  },
  {
    id: 10,
    name: "Avrit Bingham",
    desc: "Displaced transverse fracture of shaft of humerus, left arm, sequela",
    image: "https://randomuser.me/api/portraits/women/35.jpg",
  },
  {
    id: 11,
    name: "Read Rowena",
    desc: "Pressure ulcer of left heel, unstageable",
    image: "https://randomuser.me/api/portraits/women/30.jpg",
  },
  {
    id: 12,
    name: "Brenna Lavis",
    desc: "External constriction of scrotum and testes, sequela",
    image: "https://randomuser.me/api/portraits/men/71.jpg",
  },
  {
    id: 13,
    name: "Isaak Pontin",
    desc: "Xeroderma of right upper eyelid",
    image: "https://randomuser.me/api/portraits/men/50.jpg",
  },
  {
    id: 14,
    name: "Grace Bleacher",
    desc: "Other mechanical complication of implanted urinary sphincter, sequela",
    image: "https://randomuser.me/api/portraits/women/20.jpg",
  },
  {
    id: 15,
    name: "Randy Anten",
    desc: "Unspecified car occupant injured in collision with other nonmotor vehicle in traffic accident, sequela",
    image: "https://randomuser.me/api/portraits/men/15.jpg",
  },
  {
    id: 16,
    name: "Alaster Blodg",
    desc: "Other fracture of first metacarpal bone, left hand",
    image: "https://randomuser.me/api/portraits/women/55.jpg",
  },
  {
    id: 17,
    name: "Tova Baldacchino",
    desc: "Atheroembolism of bilateral upper extremities",
    image: "https://randomuser.me/api/portraits/women/60.jpg",
  },
  {
    id: 18,
    name: "Jesselyn Cantillon",
    desc: "Deprivation amblyopia, unspecified eye",
    image: "https://randomuser.me/api/portraits/men/58.jpg",
  },
  {
    id: 19,
    name: "Bendite Normandale",
    desc: "Person boarding or alighting a car injured in collision with pick-up truck",
    image: "https://randomuser.me/api/portraits/men/66.jpg",
  },
  {
    id: 20,
    name: "Lyn Steynor",
    desc: "Generalized abdominal rigidity",
    image: "https://randomuser.me/api/portraits/women/70.jpg",
  }
];
