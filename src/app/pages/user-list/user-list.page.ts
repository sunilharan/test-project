import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { SearchService } from "src/app/services/search.service";
import { User } from "src/app/shared/interfaces/user.interface";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.page.html',
  styleUrls: ['./user-list.page.scss'],
})
export class UserListPage implements OnInit {
  userList: User[];
  searchText = "";
  constructor(private searchService: SearchService, private router: Router) {}

  ngOnInit() {
    this.loadList();
  }
  loadList() {
    this.searchService.searchList$.subscribe((list) => {
      this.userList = list;
    });
  }
  openSearch(search: HTMLInputElement) {
    search.focus();
    this.searchText = "";
    this.search();
  }

  search() {
    this.searchService.search("name", this.searchText);
  }

  detail(item: User) {
    this.router.navigate(["/detail/" + item.id]);
  }
}
