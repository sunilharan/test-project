import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { SearchService } from "src/app/services/search.service";
import { User } from "src/app/shared/interfaces/user.interface";

@Component({
  selector: "app-detail",
  templateUrl: "./detail.page.html",
  styleUrls: ["./detail.page.scss"],
})
export class DetailPage implements OnInit {
  userDetail: User;
  constructor(
    private route: ActivatedRoute,
    private searchService: SearchService
  ) {
    const id = this.route.snapshot.paramMap.get("id");
    if (id) {
      this.loadUserInfo(parseInt(id));
    }
  }
  loadUserInfo(id) {
    const userList = this.searchService.searchList$.value;
    this.userDetail = userList.find((user) => user.id === id);
  }

  ngOnInit() {}
}
