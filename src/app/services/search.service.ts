import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../shared/interfaces/user.interface';
import { userList } from '../shared/utils/common-const';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  searchList$ :BehaviorSubject<User[]> = new BehaviorSubject(userList) ;
  constructor() { }

  search(key, searchValue) {
    let list = [];
    if (!searchValue) {
      list = userList;
    }
    list = userList.filter((element) => {
      return element[key].match(new RegExp(searchValue, "i"))
    });
    this.searchList$.next(list);
  }
}
