import { browser, By } from 'protractor';
import { protractor } from 'protractor/built/ptor';
import { UserListScreen } from './page-object/pages/user-list.po';

describe('User list Screen', () => {
    let page: UserListScreen;

    beforeAll(async () => {
        page = new UserListScreen();
        await page.get();
    });

    it('should display first start up page at the start', async () => {
        await browser.driver.sleep(200);
        await page.waitForElement(page.$.userItem(0), 5000);
    });

    describe('search user', () => {
        it('user not found', async () => {
            await page.lazyClick(page.$.searchForm);
            await page.fillSearchInput('lyn');
            expect(await page.$.notFound.isPresent()).toBe(false);

        });
        it('user found', async () => {
            await page.lazyClick(page.$.searchForm);
            await page.fillSearchInput('lyn');
            expect(await page.$.userItem(0).isPresent()).toBe(true);
        });
    });

    describe('should allows navigating to', () => {
        it('user detail', async () => {
            await page.lazyClick(page.$.userItem(0));
            await browser.wait(protractor.ExpectedConditions.urlContains('/detail'), 3000);
        });
    });
});
