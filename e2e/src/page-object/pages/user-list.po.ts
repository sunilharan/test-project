import { browser, by, element, $, $$ } from 'protractor';
import { BasePage } from '../base.po'

export class UserListScreen extends BasePage {
    // Selectors
    readonly selector = 'app-user-list';
    $ = {
        searchForm: $(`${this.selector} .search-form`),
        searchInput: $(`${this.selector} #search-item`),
        userItem: index => $(`${this.selector} #user-item${index}`),
        notFound: $(`${this.selector} #not-found`),
    };

    // Get page
    get() {
        return this.navigateTo('/');
    }

    async reset() {
        await this.get();
    }

    async fillSearchInput(value) {
        await this.ifAvailable(this.$.searchInput).then(async field => {
            await field.clear().sendKeys(value);
        });
    }
}
