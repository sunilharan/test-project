import { browser, ExpectedConditions, $, By } from 'protractor';

export class BasePage {

  resetAppStartTimes: boolean = true;

  constructor() {
    // Configuration
    browser
      .manage()
      .window()
      // iPhone X
      .setSize(375, 812);
  }

  async navigateTo(url: string) {
    if (url) {
      await browser.get(url);
    } else {
      await browser.get('/');
    }
    return;

  }

  async lazyClick(el) {
    await browser.driver.sleep(400);
    await el.click();
    await browser.driver.sleep(500);
  }

  async waitForElement(el, timeout = 200) {
    await browser.wait(
      ExpectedConditions.presenceOf(el),
      timeout,
      'Element is not rendered yet'
    );
    // wait few ms to ensure all dynamic content (eg. translations)
    // finished loading...as elements might be there but not the text itself
    await browser.driver.sleep(200);
  }

  async waitForElementToBeInvisible(el, timeout = 200) {
    await browser.wait(
      ExpectedConditions.invisibilityOf(el),
      timeout,
      'Element is not invisible yet'
    );
    await browser.driver.sleep(200);
  }

  async scrollToElement(el) {
    await browser.driver.executeScript("arguments[0].scrollIntoView(true);", el.getWebElement());
  }

  async back() {
    const backBtn = $('ion-footer .back');

    await this.lazyClick(backBtn.click());
  }

  async next() {
    const nextBtn = $('ion-footer .next');

    await this.lazyClick(nextBtn.click());
  }

  clearStorage() {
    browser.executeScript('window.localStorage.clear();');
  }

  async ifAvailable(element) {
    if (!element) {
      return Promise.reject('Element not found');
    }

    const enabled = await element.isEnabled();
    if (enabled) {
      return Promise.resolve(element);
    }
    return Promise.reject('Element is disabled');
  }

  clickById(id) {
    browser.driver.findElement(By.xpath(`//*[@id=\"${id}\"]`)).click().catch();
  }

}
