const { protractor } = require('protractor');

module.exports = () => {

  // cssDeep selector is broken in protractor due to /deep/ being deprecated.
  // This is workaround as found on: https://github.com/angular/protractor/issues/4367#issue-240858568
  
  /**
   * Usage:
   *   O  element(by.cssShadow('#parentElement #innerElement'))          <=> $('#parentElement #innerElement')
   *   O  element(by.cssShadow('#parentElement::sr #innerElement'))      <=> $('#parentElement').shadowRoot.$('#innerElement')
   *   O  element.all(by.cssShadow('#parentElement .inner-element'))     <=> $$('#parentElement .inner-element')
   *   O  element.all(by.cssShadow('#parentElement::sr .inner-element')) <=> $$('#parentElement').shadowRoot.$$('.inner-element')
   *   O  parentElement.element(by.cssShadow('#innerElement'))           <=> parentElement.$('#innerElement')
   *   O  parentElement.element(by.cssShadow('::sr #innerElement'))      <=> parentElement.shadowRoot.$('#innerElement')
   *   O  parentElement.all(by.cssShadow('.inner-element'))              <=> parentElement.$$('.inner-element')
   *   O  parentElement.all(by.cssShadow('::sr .inner-element'))         <=> parentElement.shadowRoot.$$('.inner-element')
   */
  protractor.by.addLocator(
    'cssShadow',
    (cssSelector, optParentElement, optRootSelector) => {
      const selectors = cssSelector.split('::sr');
      if (selectors.length === 0) {
        return [];
      }

      const shadowDomInUse = document.head.attachShadow;
      const getShadowRoot = el => (el && shadowDomInUse ? el.shadowRoot : el);
      const findAllMatches = (selector, targets, firstTry) => {
        const matches = [];
        for (const target of targets) {
          const using = firstTry ? target : getShadowRoot(target);
          if (using) {
            if (selector === '') {
              matches.push(using);
            } else {
              Array.prototype.push.apply(
                matches,
                using.querySelectorAll(selector)
              );
            }
          }
        }
        return matches;
      };

      let matchesDom = findAllMatches(
        selectors.shift().trim(),
        [optParentElement || document],
        true
      );
      while (selectors.length > 0 && matchesDom.length > 0) {
        matchesDom = findAllMatches(
          selectors.shift().trim(),
          matchesDom,
          false
        );
      }
      return matchesDom;
    }
  );
};
